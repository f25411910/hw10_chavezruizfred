# Práctica 10

**Universitario:** Fred Chavez Ruiz  
**Materia:** Diseño y Programación Gráfica

En la presente practica se debe elegir el template en el que trabajaremos en las clases de auxiliatura, los sacammos de la pagina freebiesgug(https://freebiesbug.com/figma-freebies/), del cual yo escogi:
### Template 1
![**Template 1**](/images_10/template1.png)
(https://www.figma.com/file/fP7QARhMIG3lDIbFIhJXnS/Project-Managemt-Website-Design-UI-UX-(Community)?type=design&node-id=0-1&mode=design&t=39Q8RG2lhxrTDny5-0)

Si en algun caso alguno de mis compañeros escogiera ell primer template estaria usando el siguiente:

### Template 2
![**Template 2**](/images_10/template2.png)
(https://www.figma.com/file/TBNppehnHSXTvQzV2fCl8u/E-Commerce-Plant-Shop-Website-(Community)?type=design&node-id=0-1&mode=design&t=IlhitikSJmiduCkT-0)


El repositorio donde realizare las practicas de React se llevaran en el siguiente repositorio(https://gitlab.com/f25411910/react_with_fredchavezruiz.git)